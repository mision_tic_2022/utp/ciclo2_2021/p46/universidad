public class Facultad {
    /**
     * Atributos
     */
    private String nombre;
    private Universidad universidad;

    /**
     * Constructor
     */
    public Facultad(String nombre, Universidad universidad){
        this.nombre = nombre;
        this.universidad = universidad;
    }

    /**
     * Método consultor
     * (getters)
     */
    public String getNombre(){
        return this.nombre;
    }

    /**
     * Método modificador
     * (setters)
     */
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    /**
     * Métodos
     * (Acciones de la clase)
     */
    public void crear_carreras(String nombre){
        System.out.println("Crear carrera "+nombre);
    }
    
}
