public class App {
    public static void main(String[] args) throws Exception {
        //Crear objeto universidad
        Universidad objUniversidad = new Universidad("UTP", "123456");
        //Crear facultades
        for(int i = 0; i < 5; i++){
            objUniversidad.crear_facultad("Facultad-"+i, i);
        }
        //Consultar facultades
        for(int i = 0; i < 5; i++){
            String nombre = objUniversidad.getFacultades(i).getNombre();
            System.out.println(nombre);
        }
    }
}
